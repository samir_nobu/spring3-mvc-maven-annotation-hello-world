package com.mkyong.web.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;

public class AppTest {

WebDriver driver;

	@Test	
      public void testCalc() {
	System.setProperty("webdriver.gecko.driver","/home/ub/Downloads/geckodriver");
	driver = new FirefoxDriver();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      // driver.get("http://www.google.com");
      driver.get("http://localhost:8080/spring3-mvc-maven-annotation-hello-world/");
      
      WebElement search_bar = driver.findElement(By.name("q"));
      //  search_bar.sendKeys("CPDOF");
      search_bar.click();

      driver.navigate().back();

      search_bar = driver.findElement(By.name("r"));
      search_bar.click();

      driver.navigate().back();

      search_bar = driver.findElement(By.name("s"));
      search_bar.click();

      driver.navigate().back();

       try 
 	{
		Thread.sleep(3000);
	}
	catch(Exception e)
	{
		// System.out.println(e.getMessage());
	}

      // String search_text = "I'm Feeling Lucky";

      // WebElement search_button = driver.findElement(By.xpath("/html/body/div/div[3]/form/div[2]/div/div[2]/div[2]/div/center/input[2]"));
 
      // String text = search_button.getAttribute("value");

	// System.out.println("text =" + text);
     // search_button.click();

       assertEquals("Comparision Result", 1, 1);

	//Logger log = Logger.getLogger(LoggingObject.class);
	//log.info("I'm starting");
	//log.info("mycalc add =" + myCalc.add());
	//log.info("text =" + text);

// adding a comment before quit
	driver.quit();

      }

}
